package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.clientXML.ClientFindInfo;
import org.example.clientXML.ClientInfo;
import org.example.clientXML.Dul;
import org.example.perconJSON.PersonInfo;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class JaksonExecuterTest {

    JaksonExecuter jaksonExecuter = new JaksonExecuter();

    @Test
    void getClientInfoFromFile() throws IOException {
        assertEquals(getPrepareObject(), jaksonExecuter.getClientInfoFromFile("ClientInfo.xml"));
    }

    @Test
    void savePersonInfoObjectInFile() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        PersonInfo personInfoExpected = objectMapper.readValue(ClassLoader.getSystemClassLoader().getResourceAsStream("PersonInfoTest.json"), PersonInfo.class);
        jaksonExecuter.savePersonInfoObjectInFile(personInfoExpected, "PersonInfo.json");
        File file = new File("PersonInfo.json");
        PersonInfo personInfoActual = objectMapper.readValue(file, PersonInfo.class);
        assertTrue(new File("PersonInfo.json").canExecute());
        assertEquals(personInfoExpected, personInfoActual);
    }

    private ClientInfo getPrepareObject() {
        return new ClientInfo(
                new ClientFindInfo(
                        new Dul(99, "firstName", "lastName",
                                "secondName", 645354, 6200), 32)
        );
    }
}