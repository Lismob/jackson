package org.example.perconJSON;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;


@EqualsAndHashCode
public class PersonInfo {
    @JsonProperty("FindPersonInfo")
    private FindPersonInfo findPersonInfo;

    public PersonInfo(FindPersonInfo findPersonInfo) {
        this.findPersonInfo = findPersonInfo;
    }

    public PersonInfo() {
    }

    public FindPersonInfo getFindPersonInfo() {
        return findPersonInfo;
    }
}
