package org.example;

import org.example.clientXML.ClientInfo;
import org.example.perconJSON.FindPersonInfo;
import org.example.perconJSON.IdentityCard;
import org.example.perconJSON.PersonInfo;
import org.example.perconJSON.PersonName;

public class FactoryPerson {

    private final ClientInfo clientInfo;

    public FactoryPerson(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public PersonInfo createPersonInfo() {
        return new PersonInfo(createFindPersonInfo());
    }

    public FindPersonInfo createFindPersonInfo() {
        return new FindPersonInfo(createPersonName(), createIdentityCard(), clientInfo.getClientFindInfo().getPartyId());
    }

    public PersonName createPersonName() {
        return new PersonName(
                clientInfo.getClientFindInfo().getDul().getLastName(),
                clientInfo.getClientFindInfo().getDul().getFirstName(),
                clientInfo.getClientFindInfo().getDul().getSecondName()
        );
    }

    public IdentityCard createIdentityCard() {
        return new IdentityCard(
                clientInfo.getClientFindInfo().getDul().getDocumentType(),
                clientInfo.getClientFindInfo().getDul().getSeries(),
                clientInfo.getClientFindInfo().getDul().getNumber()
        );
    }
}