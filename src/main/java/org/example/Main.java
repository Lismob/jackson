package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.example.clientXML.ClientInfo;
import org.example.perconJSON.PersonInfo;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        JaksonExecuter jaksonExecuter = new JaksonExecuter();
        FactoryPerson factoryPerson = new FactoryPerson(jaksonExecuter.getClientInfoFromFile("ClientInfo.xml"));
        jaksonExecuter.savePersonInfoObjectInFile(factoryPerson.createPersonInfo(), "PersonInfo.json");
    }
}
