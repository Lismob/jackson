package org.example.clientXML;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;

@JacksonXmlRootElement(localName = "dul")
public class Dul {
    private int documentType;
    private String firstName;
    private String lastName;
    private String secondName;
    private int number;
    private int series;

    public Dul(int documentType, String firstName, String lastName, String secondName, int number, int series) {
        this.documentType = documentType;
        this.firstName = firstName;
        this.lastName = lastName;
        this.secondName = secondName;
        this.number = number;
        this.series = series;
    }

    public Dul() {
    }

    public int getDocumentType() {
        return documentType;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getNumber() {
        return number;
    }

    public int getSeries() {
        return series;
    }

    @Override
    public String toString() {
        return "Dul{" +
                "documentType=" + documentType +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", number=" + number +
                ", series=" + series +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dul dul = (Dul) o;
        return documentType == dul.documentType && number == dul.number && series == dul.series && Objects.equals(firstName, dul.firstName) && Objects.equals(lastName, dul.lastName) && Objects.equals(secondName, dul.secondName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(documentType, firstName, lastName, secondName, number, series);
    }
}
