package org.example.clientXML;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;

@JacksonXmlRootElement(localName = "clientInfo")
public class ClientInfo {
    private ClientFindInfo clientFindInfo;

    public ClientInfo(ClientFindInfo clientFindInfo) {
        this.clientFindInfo = clientFindInfo;
    }

    public ClientInfo() {
    }

    public ClientFindInfo getClientFindInfo() {
        return clientFindInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientInfo that = (ClientInfo) o;
        return Objects.equals(clientFindInfo, that.clientFindInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientFindInfo);
    }

    @Override
    public String toString() {
        return "ClientInfo{" +
                "clientFindInfo=" + clientFindInfo +
                '}';
    }
}
