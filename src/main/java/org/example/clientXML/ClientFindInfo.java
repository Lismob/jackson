package org.example.clientXML;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.Objects;

@JacksonXmlRootElement(localName = "clientFindInfo")
public class ClientFindInfo {
    private Dul dul;
    private int partyId;

    public ClientFindInfo(Dul dul, int partyId) {
        this.dul = dul;
        this.partyId = partyId;
    }

    public ClientFindInfo() {
    }

    public Dul getDul() {
        return dul;
    }

    public int getPartyId() {
        return partyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientFindInfo that = (ClientFindInfo) o;
        return partyId == that.partyId && Objects.equals(dul, that.dul);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dul, partyId);
    }

    @Override
    public String toString() {
        return "ClientFindInfo{" +
                "dul=" + dul +
                ", partyId=" + partyId +
                '}';
    }
}
