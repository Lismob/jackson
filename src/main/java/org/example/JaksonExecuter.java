package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.example.clientXML.ClientInfo;
import org.example.perconJSON.PersonInfo;

import java.io.File;
import java.io.IOException;

public class JaksonExecuter {

    public ClientInfo getClientInfoFromFile(String filePath) throws IOException {
        File file = new File(filePath);
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.readValue(file, ClientInfo.class);
    }
    public void savePersonInfoObjectInFile(PersonInfo personInfo, String filePath) throws IOException {
        File file = new File(filePath);
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, personInfo);
    }
}
